I manage the Developer Advocacy team at GitLab. More information about the team can be found in the [Developer Advocacy](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/) section of the GitLab handbook. 

I keep an updated [README](https://coghlan.me/readme/) on my personal blog at [coghlan.me](https://coghlan.me).
